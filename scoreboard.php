<?php
    // utilise Composer, et la libraire requests
    require "vendor/autoload.php";

    $response = Requests::get('http://api.minecraft.underfight.fr/api/players');
    $playersData = json_decode($response->body);
    $players = array();
    foreach ($playersData as $player) {
        // compte les morts
        $nbDeaths = $player->total_deaths;
        array_push($players, array('uuid'=>$player->uuid, 'username'=>$player->username, 'deaths'=>$nbDeaths, 'head'=>$player->uri . 'avatar'));
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Scoreboard</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/styles.css">
    </head>
    <body>
        <div class="container">
            <div class="d-flex justify-content-center h-100">
                <div class="card">
                    <div class="card-header">
                        <h3>Scoreboard</h3>
                    </div>
                    <div class="card-body table-wrapper-scroll-y">
                        <table class="table">
                            <tbody>
                                <?php
                                    foreach ($players as $player) {
                                        echo '<tr onclick="location.href=\'profile.php?uuid=' . $player['uuid'] . '\'">';
                                        echo '<td><img src="' . $player['head'] . '" alt="head" /></td>';
                                        echo '<td>' . $player['username'] . '</td>';
                                        echo '<td>' . $player['deaths'] . ' death';
                                        if ($player['deaths'] > 1) echo 's';
                                        echo'</td>';
                                        echo '</tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>