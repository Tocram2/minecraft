FROM php:7.4-apache

RUN apt-get update && apt-get upgrade -y && apt-get autoremove -y
RUN apt-get install git --no-install-recommends -y

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN usermod -u 1000 www-data && groupmod -g 1000 www-data

COPY . /var/www/html

RUN composer install --prefer-source --no-interaction

RUN chown -R www-data:www-data /var/www/html

