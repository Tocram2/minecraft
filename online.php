<?php
    // uses Composer and the 'requests' library
    require "vendor/autoload.php";

    // username comparing function, used in usort
    function compare_usernames($a, $b) {
        return strnatcmp($a['username'], $b['username']);
    }

    $response = Requests::get('http://api.minecraft.underfight.fr/api/players/online');
    $playersData = json_decode($response->body);
    $players = array();
    foreach ($playersData as $player) {
        array_push($players, array('uuid'=>$player->uuid, 'username'=>$player->username, 'head'=>$player->uri . 'avatar', 'health'=>$player->health));
    }
    usort($players, 'compare_usernames');
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Online players</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/styles.css">
    </head>
    <body>
        <div class="container">
            <div class="d-flex justify-content-center h-100">
                <div class="card">
                    <div class="card-header">
                        <h3>Online players (<?php echo count($players); ?>)</h3>
                    </div>
                    <div class="card-body table-wrapper-scroll-y">
                        <table class="table">
                            <tbody>
                                <?php
                                    foreach ($players as $player) {
                                        echo '<tr onclick="location.href=\'profile.php?uuid=' . $player['uuid'] . '\'">';
                                        echo '<td rowspan="2"><img src="' . $player['head'] . '" alt="head" class="heads"/></td>';
                                        echo '<td>' . $player['username'];
                                        echo '</tr><tr>';

                                        // code for displaying hearts
                                        echo '<td><div class="hearts_line">';
                                        $health = $player['health'] / 2;        // makes the life go up to 10 instead of 20
                                        for ($counter = 0; $counter < 10; $counter++) {
                                            $heartType = "";
                                            if ($health > 0) {                  // if there's life left
                                                if ($health % 1 != $health) {       // if there's at least 1 heart left
                                                    $health--;
                                                    $heartType = 'Heart';
                                                }
                                                else {                              // if there's only half a heart left
                                                    $health--;
                                                    $heartType = 'Half_Heart';
                                                }
                                            }
                                            else {                              // if there's no life left
                                                $heartType = 'Empty_Heart';
                                            }
                                            echo '<img src="images/' . $heartType . '.svg" class="heart"/>';
                                        }
                                        echo '</div></td>';

                                        echo '</tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>