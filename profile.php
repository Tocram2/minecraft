<?php
    // uses Composer and the 'requests' library
    require "vendor/autoload.php";

    $uuid = $_GET['uuid'];
    $response = Requests::get('http://api.minecraft.underfight.fr/api/player/' . $uuid);
    $playersData = json_decode($response->body);
    $player = array('username'=>$playersData->username, 'texture'=>$playersData->texture,
        'deaths'=>$playersData->total_deaths, 'last_seen'=>$playersData->last_seen);
?>

<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $player['username']; ?>'s profile</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <link rel="stylesheet" type="text/css" href="css/style_player.css">

        <!-- Skin Viewer Stylesheet --->
	    <link rel="stylesheet" href="minecraft-skinviewer.css">
    </head>
    <body>
        <div class="container">
            <div class="d-flex justify-content-center h-100">
                <div class="card">
                    <div class="card-header">
                        <h3><?php echo $player['username']; ?>'s profile</h3>
                    </div>
                    <div class="card-body">
                        <div class="wrapper">
                            <!-- Set Skin for the Viewer -->
                            <style>
                                #skin-viewer-2 * { background-image: url(<?php echo $player['texture']; ?>); }
                            </style>

                            <!-- Skin Viewer HTML Elements -->
                            <div id="skin-viewer-2" class="mc-skin-viewer-11x legacy legacy-cape spin">
                                <div class="player">
                                    <!-- Head -->
                                    <div class="head" >
                                        <div class="top"></div>
                                        <div class="left"></div>
                                        <div class="front"></div>
                                        <div class="right"></div>
                                        <div class="back"></div>
                                        <div class="bottom"></div>
                                        <div class="accessory">
                                            <div class="top"></div>
                                            <div class="left"></div>
                                            <div class="front"></div>
                                            <div class="right"></div>
                                            <div class="back"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                    <!-- Body -->
                                    <div class="body">
                                        <div class="top"></div>
                                        <div class="left"></div>
                                        <div class="front"></div>
                                        <div class="right"></div>
                                        <div class="back"></div>
                                        <div class="bottom"></div>
                                        <div class="accessory">
                                            <div class="top"></div>
                                            <div class="left"></div>
                                            <div class="front"></div>
                                            <div class="right"></div>
                                            <div class="back"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                    <!-- Left Arm -->
                                    <div class="left-arm">
                                        <div class="top"></div>
                                        <div class="left"></div>
                                        <div class="front"></div>
                                        <div class="right"></div>
                                        <div class="back"></div>
                                        <div class="bottom"></div>
                                        <div class="accessory">
                                            <div class="top"></div>
                                            <div class="left"></div>
                                            <div class="front"></div>
                                            <div class="right"></div>
                                            <div class="back"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                    <!-- Right Arm -->
                                    <div class="right-arm">
                                        <div class="top"></div>
                                        <div class="left"></div>
                                        <div class="front"></div>
                                        <div class="right"></div>
                                        <div class="back"></div>
                                        <div class="bottom"></div>
                                        <div class="accessory">
                                            <div class="top"></div>
                                            <div class="left"></div>
                                            <div class="front"></div>
                                            <div class="right"></div>
                                            <div class="back"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                    <!-- Left Leg -->
                                    <div class="left-leg">
                                        <div class="top"></div>
                                        <div class="left"></div>
                                        <div class="front"></div>
                                        <div class="right"></div>
                                        <div class="back"></div>
                                        <div class="bottom"></div>
                                        <div class="accessory">
                                            <div class="top"></div>
                                            <div class="left"></div>
                                            <div class="front"></div>
                                            <div class="right"></div>
                                            <div class="back"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                    <!-- Right Leg -->
                                    <div class="right-leg">
                                        <div class="top"></div>
                                        <div class="left"></div>
                                        <div class="front"></div>
                                        <div class="right"></div>
                                        <div class="back"></div>
                                        <div class="bottom"></div>
                                        <div class="accessory">
                                            <div class="top"></div>
                                            <div class="left"></div>
                                            <div class="front"></div>
                                            <div class="right"></div>
                                            <div class="back"></div>
                                            <div class="bottom"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="player-info">
                                <p><?php echo $player['deaths'] . ' death';
                                if ($player['deaths'] > 1) echo 's'; ?></p>
                                <p>Last seen on : <?php
                                    $d = new DateTime($player['last_seen']);
                                    echo $d->format('D jS M Y, G:i:s');
                                ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>