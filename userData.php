<?php
    require "vendor/autoload.php";
    header('Content-Type: application/json');

    // get player's associated data
    $responsePlayerData = Requests::get('http://api.minecraft.underfight.fr/api/players/' . $_GET['uuid']);
    $playerData = json_decode($responsePlayerData->body);

    // sum the times the player died on each server
    $nbDeaths = 0;
    foreach ($playerData->scores as $serverData) {
        $nbDeaths += $serverData->value;
    }

    // ouput the data we need in a JSON
    $data = array('uuid'=>$uuid, 'deaths'=>$nbDeaths);
    echo json_encode($data);
?>